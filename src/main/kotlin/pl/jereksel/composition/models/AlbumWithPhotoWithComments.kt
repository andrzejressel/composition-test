package pl.jereksel.composition.models

import java.util.*

class AlbumWithPhotoWithComments(
    val id: String,
    val name: String,
    val userId: String,
    val creationDate: Date,
    val linkedAlbums: List<String>,
    val photos: List<PhotoWithComments>
) {

    fun setPhotosWithComments(photos: List<PhotoWithComments>) = AlbumWithPhotoWithComments(
        id = id,
        name = name,
        userId = userId,
        creationDate = creationDate,
        linkedAlbums = linkedAlbums,
        photos = photos
    )

}