package pl.jereksel.composition.models

class Comment(
    val id: String,
    val user: String,
    val text: String
)