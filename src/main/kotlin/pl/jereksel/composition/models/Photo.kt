package pl.jereksel.composition.models

data class Photo(
    val id: String,
    val name: String,
    val url: String,
    val commentIds: List<String>
) {

    fun setComments(comments: List<Comment>) = PhotoWithComments(
        id = id,
        name = name,
        url = url,
        comments = comments
    )

}